# Install Rmate

- `wget -O /usr/local/bin/rmate https://raw.github.com/aurora/rmate/master/rmate`
- `chmod a+x /usr/local/bin/rmate`

# Automatic Updates

- [Manual](https://linuxiac.com/how-to-set-up-automatic-updates-on-debian/)
- as root
- `apt update`
- `apt install unattended-upgrades`
- `rmate /etc/apt/apt.conf.d/50unattended-upgrades` - make all edits suggested in the link
- `dpkg-reconfigure --priority=low unattended-upgrades`

# Setup starting AVD via SSH

- `apt install ipmitool`
- `adduser --disabled-password power`
- Copy over the folders `.ssh` and `bin` to `~power`

# TODO

- Setup administrative access to ipmi tools and remote console via java web start, possibly via docker, using https://github.com/DomiStyle/docker-idrac6 as template