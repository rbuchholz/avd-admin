from datetime import datetime, timedelta
import io
import tempfile
import subprocess
import sys

import unittest
from unittest import mock
from pyexpect import expect
import fluentpy as _

import auto_shutdown

def focus(test):
    test.has_focus = True
    return test

def load_tests(loader, tests, pattern):
    def is_fokused(test_case):
        return _(test_case).getattr(test_case._testMethodName).getattr('has_focus', False)._
    
    tests = loader.loadTestsFromTestCase(AcceptanceTest)
    
    if _(tests).map(is_fokused).any()._:
        tests_subset = _(tests).filter(is_fokused)._
        return _(unittest.TestSuite()).addTests(tests_subset).self._
    
    return tests


class AcceptanceTest(unittest.TestCase):
    
    def setUp(self):
        super()
        
        # mock stdtout
        self.mock_stdout = io.StringIO()
        sys.stdout = self.mock_stdout
        def undo_stdout_mock():
            sys.stdout = sys.__stdout__
        self.addCleanup(undo_stdout_mock)
        
        # mock timestamp file
        self.temporary_file = tempfile.NamedTemporaryFile('r+')
        original_timestamp_file_path = auto_shutdown.TIMESTAMP_FILE_PATH
        auto_shutdown.TIMESTAMP_FILE_PATH = self.temporary_file.name
        def undo_timestamp_mock():
            auto_shutdown.TIMESTAMP_FILE_PATH = original_timestamp_file_path
        self.addCleanup(undo_timestamp_mock)
        
        # mock subprocess.run
        original_subprocess_run = subprocess.run
        self.mock_run = subprocess.run = mock.MagicMock()
        def undo_subprocess_run_mock():
            subprocess.run = original_subprocess_run
        self.addCleanup(undo_subprocess_run_mock)
    
    def test_shows_help(self):
        auto_shutdown.main(['', '-h'])
        expect(self.mock_stdout.getvalue()).contains('Usage')
    
    def test_writes_shutdown_time_into_control_file_if_invoked_with_relative_time(self):
        auto_shutdown.main(['', '1', 'hour'])
        
        read_date = datetime.fromisoformat(self.temporary_file.readline().strip())
        now = datetime.now()
        expect(read_date).is_between(now + timedelta(minutes=50), now + timedelta(minutes=70))
    
    def test_should_print_to_stdout_when_the_shutdown_is_scheduled(self):
        auto_shutdown.main(['', '1 hour'])
        expect(self.mock_stdout.getvalue().splitlines()[0]).contains('Scheduling shutdown in: 1')
    
    def test_shows_help_below_first_line_of_control_file(self):
        expect(self.temporary_file.read()) == ''
        auto_shutdown.main(['', '1 hour'])
        read_lines = '\n'.join(self.temporary_file.readlines()[1:])
        expect(read_lines).contains('auto-shutdown $some_human_readable_time')
    
    def test_extends_shutdown_time_when_called_with_no_parameters_and_users_are_logged_in(self):
        self.mock_run.return_value = mock.Mock(stdout=b'dwt dwt\n# users=2')
        
        auto_shutdown.main([''])
        
        read_date = datetime.fromisoformat(self.temporary_file.readlines()[0].strip())
        now = datetime.now()
        expect(read_date).is_between(now + timedelta(minutes=50), now + timedelta(minutes=70))
    
    def test_triggers_shutdown_when_called_with_no_parameters_and_no_users_are_logged_in(self):
        self.mock_run.return_value = mock.Mock(stdout=b'\n# users=0')
        auto_shutdown.schedule_shutdown(datetime.now() - timedelta(hours=1), should_constrain=False)
        
        auto_shutdown.main([''])
        # self.fail(self.mock_run.call_args_list)
        self.mock_run.assert_any_call(['who', '-q'], env=mock.ANY, capture_output=True, check=True)
        self.mock_run.assert_any_call(['systemctl', 'poweroff'], check=True)
    
    def test_should_show_error_if_date_cannot_be_parsed(self):
        error = expect(
            lambda: auto_shutdown.main(['something', 'not', 'parseable'])
        ).to_raise(AssertionError, r"Cannot parse: \['something', 'not', 'parseable'\].*")
    

if __name__ == '__main__':
    unittest.main(argv=[__file__])
