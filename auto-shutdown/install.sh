#!/bin/sh

cd "$(dirname "$0")"
HERE="$(pwd)"

if test ! -d virtualenv/
then
    python3 -m venv virtualenv
fi

virtualenv/bin/pip install --upgrade pip wheel setuptools
virtualenv/bin/pip install -r requirements.txt

cat <<END > /usr/local/bin/auto_shutdown
#!/bin/sh
LC_ALL=C $HERE/virtualenv/bin/python $HERE/auto_shutdown.py \$*
END

chmod a+x /usr/local/bin/auto_shutdown

cp auto_shutdown.timer auto_shutdown.service /etc/systemd/system/
systemctl enable --now auto_shutdown.timer
