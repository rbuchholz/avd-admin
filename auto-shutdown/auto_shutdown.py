#!/usr/bin/env python
"""
Auto shutdown control and cron script.

Usage:

auto-shutdown $some_human_readable_time
    This sets the given time as the shutdown time.
    Can be no more than one week into the future.
    Human readable includes formulations like 'tomorrow' in multiple languages.
    See https://dateparser.readthedocs.io/en/latest/index.html for details.

auto-shutdown
    Checks wether users are logged in and extends the shutdown time by at least one hour
    If no user is logged in, and the previously set shutdown time has been reached, it shuts down the machine.
    It also checks the current shutdown time and ennsures it is no more than one week in the future.
    If you do something that requires more time, you will have to find time in that week to login again and extend the time.
"""

"""
# What problem is this trying to solve?
- Our server uses quite a lot of power, even if it is not running.
- To counteract this, it is by default off and can be remote started (via SSH)
- Now the server should self turn off if nobody is using it
- Except for longer compute-jobs there should be a way to keep it on for some time even if not logged in
- That time will be constrained, so you have to log back in in that period to extend the on-time again

# How is this supposed to work?
- If you invoke this tool periodically as root, it will check wether users are logged in.
- If yes, it will do nothing and wait for the next invocation
- If no
    - check wether some time is requested for a compute job (accepting at most one week) and quit
    - or turn off the machine
- So it doesn't auto-shutdown the
"""

from datetime import datetime, timedelta
import os
import sys
import re
import subprocess

import dateparser
from babel.dates import format_timedelta
import fluentpy as _

TIMESTAMP_FILE_PATH = '/var/run/auto_shutdown.timestamp'
MAX_SHUTDOWN_DELAY = datetime.now() + timedelta(days=7)
MIN_SHUTDOWN_DELAY = datetime.now() + timedelta(hours=1)

def main(argv=sys.argv):
    if '-h' in argv:
        print(__doc__)
        return
    
    if len(argv) > 1:
        schedule_shutdown(parse_date(argv))
    elif number_of_logged_in_users() > 0:
        extend_shutdown_delay_to_at_least_one_hour()
    elif time_of_current_scheduled_shutdown() < datetime.now():
        shutdown()

def schedule_shutdown(input_date=None, *, should_constrain=True):
    shutdown_delay = input_date or datetime.now()
    if should_constrain:
        shutdown_delay = constrain_date(shutdown_delay)
    shutdown_delay_description = format_timedelta(shutdown_delay - datetime.now())
    print(f'Scheduling shutdown in: {shutdown_delay_description} ({shutdown_delay})')
    
    with open(TIMESTAMP_FILE_PATH, 'w+') as timestamp_file:
        os.chmod(TIMESTAMP_FILE_PATH, 0o777)
        timestamp_file.write(shutdown_delay.isoformat())
        timestamp_file.write('\n\n' + __doc__)

def parse_date(argv):
    shutdown_delay = _(argv)[1:].join(' ').to(dateparser.parse)
    assert shutdown_delay, f'Cannot parse: {argv!r}. \n\n{__doc__}'
    return shutdown_delay

def constrain_date(input_date, min_date=MIN_SHUTDOWN_DELAY, max_date=MAX_SHUTDOWN_DELAY):
    return min(max(min_date, input_date), max_date)

def number_of_logged_in_users():
    # consider to parse number of users instead (last line)
    last_output_line = subprocess.run(['who', '-q'], env=dict(os.environ, LC_ALL='C'), check=True, capture_output=True).stdout.splitlines()[-1]
    match = _(last_output_line).strip().decode().match(r'^# users=(\d+)$')._
    assert match, 'who output unreadable'
    return int(match.group(1))

def extend_shutdown_delay_to_at_least_one_hour():
    schedule_shutdown(time_of_current_scheduled_shutdown())

def time_of_current_scheduled_shutdown():
    'also constrains that time to at most one week and writes it back to disk if neccessaary'
    ensure_propper_shutdown_delay_file_exists()
    
    with open(TIMESTAMP_FILE_PATH) as timestamp_file:
        return dateparser.parse(timestamp_file.readline().strip())

def ensure_propper_shutdown_delay_file_exists():
    if not  os.path.exists(TIMESTAMP_FILE_PATH):
        schedule_shutdown()
    if not os.access(TIMESTAMP_FILE_PATH, os.W_OK | os.R_OK):
        schedule_shutdown()
    os.chmod(TIMESTAMP_FILE_PATH, 0o777)
    
    with open(TIMESTAMP_FILE_PATH) as timestamp_file:
        read_date = dateparser.parse(timestamp_file.readline().strip())
    
    if not read_date:
        schedule_shutdown()
    elif read_date > MAX_SHUTDOWN_DELAY:
        schedule_schutdown(read_date)

def shutdown():
    subprocess.run(['systemctl', 'poweroff'], check=True)

if __name__ == '__main__':
    main()
